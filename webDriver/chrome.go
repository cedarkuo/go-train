package webdriver

import (
	"fmt"

	"github.com/BurntSushi/toml"
	"github.com/tebeka/selenium"
)

type Config struct {
	PersonID    string
	GetinDate   string
	FromStation string
	ToStation   string
	TrainNo     string
	OrderQtyStr string
}

var conf Config

// StartWebDriver for start web
func StartWebDriver() {
	setConfig()

	const (
		// These paths will be different on your system.
		chromeDrivePath = "C:/chromedriver"
		port            = 9515
	)
	selenium.SetDebug(true)
	_, err := selenium.NewChromeDriverService(chromeDrivePath, port)
	if err != nil {
		panic(err) // panic is used only as an example and is not otherwise recommended.
	}

	// Connect to the WebDriver instance running locally.
	caps := selenium.Capabilities{"browserName": "chrome"}
	wd, err := selenium.NewRemote(caps, fmt.Sprintf("http://localhost:%d/wd/hub", port))
	if err != nil {
		panic(err)
	}

	// Locate to specific page
	wd.Get("http://railway.hinet.net/Foreign/TW/etno1.html")
	idElement, err := wd.FindElement(selenium.ByCSSSelector, "#person_id")
	if err != nil {
		panic(err)
	}
	idElement.SendKeys(conf.PersonID)

	setGetInDate(wd)

	fromStationElement, err := wd.FindElement(selenium.ByCSSSelector, "#from_station")
	if err != nil {
		panic(err)
	}
	fromStationElement.SendKeys(conf.FromStation)

	toStationElement, err := wd.FindElement(selenium.ByCSSSelector, "#to_station")
	if err != nil {
		panic(err)
	}
	toStationElement.SendKeys(conf.ToStation)

	trainNoElement, err := wd.FindElement(selenium.ByCSSSelector, "#train_no")
	if err != nil {
		panic(err)
	}
	trainNoElement.SendKeys(conf.TrainNo)

	setOrderQtyStr(wd)
}

func setOrderQtyStr(wd selenium.WebDriver) {
	orderQtyStrElement, err := wd.FindElement(selenium.ByCSSSelector, "#order_qty_str")
	if err != nil {
		panic(err)
	}

	orderQtyStrElement.SendKeys(conf.OrderQtyStr)
}

func setGetInDate(wd selenium.WebDriver) {
	// For process getinDate to fit train order page dateElement value format
	// And send value to dateElement
	dateElement, err := wd.FindElement(selenium.ByCSSSelector, "#getin_date")
	if err != nil {
		panic(err)
	}

	dateElementOption, err := dateElement.FindElements(selenium.ByCSSSelector, "option")
	if err != nil {
		panic(err)
	}

	for _, option := range dateElementOption {
		selectedOtionValue, err := option.GetAttribute("value")
		if err != nil {
			panic(err)
		}

		if selectedOtionValue[0:10] == conf.GetinDate {
			dateElement.SendKeys(selectedOtionValue)
			break
		}
	}
}

func setConfig() {
	_, err := toml.DecodeFile("config.toml", &conf)
	if err != nil {
		panic(err)
	}
}
